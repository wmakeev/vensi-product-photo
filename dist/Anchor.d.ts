export declare enum Anchor {
    TopLeft = "TopLeft",
    Top = "Top",
    TopRight = "TopRight",
    MiddleLeft = "MiddleLeft",
    Center = "Center",
    MiddleRight = "MiddleRight",
    BottomLeft = "BottomLeft",
    Bottom = "Bottom",
    BottomRight = "BottomRight"
}
export declare function getAnchor(anchor: string): Anchor;
export declare function getImageMagickGravity(anchor: Anchor): "Center" | "NorthWest" | "North" | "NorthEast" | "West" | "East" | "SouthWest" | "South" | "SouthEast";
//# sourceMappingURL=Anchor.d.ts.map