"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const blue_tape_1 = __importDefault(require("blue-tape"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const __1 = require("..");
const getCropProcess_1 = __importDefault(require("../getCropProcess"));
const IMG_FOLDER = path_1.default.resolve(__dirname, '../../img');
const TMP_FOLDER = path_1.default.resolve(__dirname, '../../__temp');
const IMG_WIDTH = 549;
const IMG_HEIGHT = 732;
const CELL_PX = 18.3;
const getMarginValue = (cells) => (cells * CELL_PX) / IMG_WIDTH;
// TODO Сделать встроенные авто-тесты по кейсам. Автоматически выделять границу и сравнивать.
blue_tape_1.default('crop #1', t => {
    var _a, _b;
    const FILE_NAME = 'graph-paper-6x8-blue';
    t.plan(1);
    const inFile = path_1.default.resolve(IMG_FOLDER, `.${FILE_NAME}.jpg`);
    const inFileStream = fs_1.default.createReadStream(inFile);
    const outFile1 = path_1.default.resolve(TMP_FOLDER, `${FILE_NAME}_01.jpg`);
    const outFile1Stream = fs_1.default.createWriteStream(outFile1);
    const sourceFrame = {
        width: IMG_WIDTH,
        height: IMG_HEIGHT,
        margin: [
            getMarginValue(15),
            getMarginValue(5),
            getMarginValue(5),
            getMarginValue(10)
        ]
    };
    const targetFrame = {
        width: Math.round(IMG_WIDTH / 2),
        height: Math.round(IMG_HEIGHT / 2),
        margin: [
            getMarginValue(4),
            getMarginValue(2),
            getMarginValue(3),
            getMarginValue(6)
        ]
    };
    const cropProcess = getCropProcess_1.default('jpg', 'jpg', sourceFrame, targetFrame, __1.Anchor.Bottom);
    (_a = cropProcess.stdout) === null || _a === void 0 ? void 0 : _a.on('close', () => t.pass());
    (_b = cropProcess.stdout) === null || _b === void 0 ? void 0 : _b.pipe(outFile1Stream);
    inFileStream.pipe(cropProcess.stdin);
});
blue_tape_1.default('crop #2 (fail)', t => {
    t.plan(1);
    const sourceFrame = {
        width: 1901,
        height: 2500,
        margin: [
            0.17674907943187795,
            0.12204103103629668,
            0.20305102577590742,
            0.12204103103629668
        ]
    };
    const targetFrame = {
        width: 1500,
        height: 2000,
        margin: [0.5, 0.5, 0.5, 0.5]
    };
    try {
        getCropProcess_1.default('jpg', 'jpg', sourceFrame, targetFrame, __1.Anchor.Bottom);
    }
    catch (err) {
        t.equal(err.message, 'Границы целевой рамки перекрывают всю область объекта', 'should fail');
    }
});
blue_tape_1.default.only('crop #3', t => {
    var _a;
    t.plan(1);
    const photoReadStream = fs_1.default.createReadStream(path_1.default.resolve(IMG_FOLDER, '65473-front.jpg'));
    const photoWriteStream = fs_1.default.createWriteStream(path_1.default.resolve(TMP_FOLDER, '65473-front-out.jpg'));
    const sourceFrame = {
        width: 1901,
        height: 2500,
        margin: [0.149, 0.151, 0.164, 0.134]
    };
    const targetFrame = {
        width: 150,
        height: 200,
        margin: [0.0, 0.0, 0.0, 0.0]
    };
    const cropProc = getCropProcess_1.default('jpg', 'jpg', sourceFrame, targetFrame, __1.Anchor.Bottom);
    (_a = cropProc.stdout) === null || _a === void 0 ? void 0 : _a.pipe(photoWriteStream);
    photoReadStream.pipe(cropProc.stdin);
    cropProc
        .then(() => {
        t.pass('should pass');
    })
        .catch(err => {
        t.fail(err.message);
    });
});
//# sourceMappingURL=getCropProcess.test.js.map