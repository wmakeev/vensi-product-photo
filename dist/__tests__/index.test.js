"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const blue_tape_1 = __importDefault(require("blue-tape"));
const __1 = require("..");
const __2 = require("..");
const IMG_FOLDER = path_1.default.resolve(__dirname, '../../img');
const TMP_FOLDER = path_1.default.resolve(__dirname, '../../__temp');
blue_tape_1.default.only('crop width gradient #1', t => {
    var _a;
    t.plan(1);
    const photoReadStream = fs_1.default.createReadStream(path_1.default.resolve(IMG_FOLDER, '65473-front.jpg'));
    const photoWriteStream = fs_1.default.createWriteStream(path_1.default.resolve(TMP_FOLDER, '65473-front-out.jpg'));
    const sourceFrame = {
        width: 1901,
        height: 2500,
        margin: [0.149, 0.151, 0.164, 0.134]
    };
    const targetFrame = {
        width: 300,
        height: 400,
        margin: [0.03, 0.06, 0.06, 0.06]
    };
    const cropProc = __2.getCropProcess({
        inputStream: photoReadStream,
        sourceFrame,
        targetFrame,
        targetAnchor: __1.Anchor.Bottom,
        applyBottomGradient: true,
        bottomGradientExand: 1.1
    });
    (_a = cropProc.stdout) === null || _a === void 0 ? void 0 : _a.pipe(photoWriteStream);
    cropProc
        .then(() => {
        t.pass('should pass');
    })
        .catch(err => {
        t.fail(err.message);
    });
});
//# sourceMappingURL=index.test.js.map