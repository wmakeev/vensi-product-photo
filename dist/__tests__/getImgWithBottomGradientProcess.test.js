"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const blue_tape_1 = __importDefault(require("blue-tape"));
const getImgWithBottomGradientProcess_1 = __importDefault(require("../getImgWithBottomGradientProcess"));
const IMG_FOLDER = path_1.default.resolve(__dirname, `../../img`);
const TMP_FOLDER = path_1.default.resolve(__dirname, `../../__temp`);
blue_tape_1.default('getImgStreamWithBottomGradient', t => {
    t.plan(1);
    const srcPhotoStream = fs_1.default.createReadStream(path_1.default.join(IMG_FOLDER, '00002-front.jpg'));
    const resultPrc = getImgWithBottomGradientProcess_1.default('jpg', 1500, 120, 'jpg');
    const writeStream = fs_1.default.createWriteStream(path_1.default.join(TMP_FOLDER, '00002-front-gradient.jpg'));
    srcPhotoStream.on('close', () => {
        t.pass('DONE');
    });
    srcPhotoStream.pipe(resultPrc.stdin);
    resultPrc.stdout.pipe(writeStream);
});
//# sourceMappingURL=getImgWithBottomGradientProcess.test.js.map