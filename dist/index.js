"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Anchor_1 = require("./Anchor");
exports.Anchor = Anchor_1.Anchor;
exports.getAnchor = Anchor_1.getAnchor;
exports.getImageMagickGravity = Anchor_1.getImageMagickGravity;
const getCropWithBottomGradientProcess_1 = __importDefault(require("./getCropWithBottomGradientProcess"));
const getCropProcess = getCropWithBottomGradientProcess_1.default;
exports.getCropProcess = getCropProcess;
//# sourceMappingURL=index.js.map