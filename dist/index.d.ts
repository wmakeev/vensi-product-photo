import { Anchor, getAnchor, getImageMagickGravity } from './Anchor';
import getCropWithBottomGradientProcess from './getCropWithBottomGradientProcess';
/**
 * Границы объекта (отступы)
 *
 * [{сверху}, {справа}, {снизу}, {слева}]
 *
 * Например: `[0.1, 0.2, 0.1, 0.2]`
 */
export declare type FrameMargin = [number, number, number, number];
export interface Frame {
    /** Ширина изображения */
    width: number;
    /** Высота изображения */
    height: number;
    /**
     * Границы объекта (отступы) в процентах от ширины минимальной стороны изображения
     *
     * [{сверху}, {справа}, {снизу}, {слева}]
     *
     * Например: `[0.1, 0.2, 0.1, 0.2]`
     */
    margin: FrameMargin;
}
export interface FramePx {
    /** Ширина изображения */
    width: number;
    /** Высота изображения */
    height: number;
    /**
     * Границы объекта (отступы) в пикселях
     *
     * [{сверху}, {справа}, {снизу}, {слева}]
     *
     * Например: `[100, 200, 100, 200]`
     */
    marginPx: FrameMargin;
}
declare const getCropProcess: typeof getCropWithBottomGradientProcess;
export { getCropProcess, Anchor, getAnchor, getImageMagickGravity };
//# sourceMappingURL=index.d.ts.map