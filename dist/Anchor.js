"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Anchor;
(function (Anchor) {
    Anchor["TopLeft"] = "TopLeft";
    Anchor["Top"] = "Top";
    Anchor["TopRight"] = "TopRight";
    Anchor["MiddleLeft"] = "MiddleLeft";
    Anchor["Center"] = "Center";
    Anchor["MiddleRight"] = "MiddleRight";
    Anchor["BottomLeft"] = "BottomLeft";
    Anchor["Bottom"] = "Bottom";
    Anchor["BottomRight"] = "BottomRight";
})(Anchor = exports.Anchor || (exports.Anchor = {}));
function getAnchor(anchor) {
    switch (anchor) {
        case 'TopLeft':
            return Anchor.TopLeft;
        case 'Top':
            return Anchor.Top;
        case 'TopRight':
            return Anchor.TopRight;
        case 'MiddleLeft':
            return Anchor.MiddleLeft;
        case 'Center':
            return Anchor.Center;
        case 'MiddleRight':
            return Anchor.MiddleRight;
        case 'BottomLeft':
            return Anchor.BottomLeft;
        case 'Bottom':
            return Anchor.Bottom;
        case 'BottomRight':
            return Anchor.BottomRight;
        default:
            throw new Error(`Unknown Anchor type ${anchor}`);
    }
}
exports.getAnchor = getAnchor;
function getImageMagickGravity(anchor) {
    switch (anchor) {
        case Anchor.TopLeft:
            return 'NorthWest';
        case Anchor.Top:
            return 'North';
        case Anchor.TopRight:
            return 'NorthEast';
        case Anchor.MiddleLeft:
            return 'West';
        case Anchor.Center:
            return 'Center';
        case Anchor.MiddleRight:
            return 'East';
        case Anchor.BottomLeft:
            return 'SouthWest';
        case Anchor.Bottom:
            return 'South';
        case Anchor.BottomRight:
            return 'SouthEast';
        default:
            throw new Error(`Unknown Anchor type ${anchor}`);
    }
}
exports.getImageMagickGravity = getImageMagickGravity;
//# sourceMappingURL=Anchor.js.map