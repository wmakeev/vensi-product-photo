"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const execa_1 = __importDefault(require("execa"));
// import { spawn } from 'child_process'
// const execa = spawn
const CONVERT = path_1.default.join(process.env.IM_PATH || '', 'convert');
function getImgWithBottomGradientProcess(srcImgFormat = 'miff', srcImgWidth, bottomGradientWidthPx, outImgFormat = 'jpg') {
    const minGradWidth = Math.round(srcImgWidth * 0.01) || 10;
    if (bottomGradientWidthPx < minGradWidth) {
        bottomGradientWidthPx = minGradWidth;
    }
    const gradientMiffStream = execa_1.default(CONVERT, [
        `-size`,
        `${Math.round(srcImgWidth)}x${Math.round(bottomGradientWidthPx)}`,
        `gradient:`,
        `miff:-`
    ]).stdout;
    const resultChildProc = execa_1.default(CONVERT, [
        `${srcImgFormat}:-`,
        'fd:3',
        '-gravity',
        'South',
        '-alpha',
        'off',
        '-compose',
        'CopyOpacity',
        '-composite',
        '-background',
        'white',
        '-alpha',
        'remove',
        `${outImgFormat}:-`
    ], {
        stdio: ['pipe', 'pipe', 2, 'pipe']
    });
    gradientMiffStream.pipe(resultChildProc.stdio[3]);
    return resultChildProc;
}
exports.default = getImgWithBottomGradientProcess;
//# sourceMappingURL=getImgWithBottomGradientProcess.js.map