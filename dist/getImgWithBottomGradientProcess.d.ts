import execa from 'execa';
export default function getImgWithBottomGradientProcess(srcImgFormat: "miff" | "png" | "jpg" | undefined, srcImgWidth: number, bottomGradientWidthPx: number, outImgFormat?: 'miff' | 'png' | 'jpg'): execa.ExecaChildProcess<string>;
//# sourceMappingURL=getImgWithBottomGradientProcess.d.ts.map