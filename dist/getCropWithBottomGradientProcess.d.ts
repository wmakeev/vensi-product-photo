/// <reference types="node" />
import { Frame, Anchor } from '.';
import { Readable } from 'stream';
declare type Options = {
    inputStream: Readable;
    inFormat?: 'miff' | 'png' | 'jpg';
    outFormat?: 'miff' | 'png' | 'jpg';
    sourceFrame: Frame;
    targetFrame: Frame;
    targetAnchor: Anchor;
    applyBottomGradient?: boolean;
    bottomGradientExand?: number;
};
export default function getCropWithBottomGradientProcess(options: Options): import("execa").ExecaChildProcess<string>;
export {};
//# sourceMappingURL=getCropWithBottomGradientProcess.d.ts.map