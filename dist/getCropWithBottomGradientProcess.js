"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const getCropProcess_1 = __importDefault(require("./getCropProcess"));
const getImgWithBottomGradientProcess_1 = __importDefault(require("./getImgWithBottomGradientProcess"));
function getCropWithBottomGradientProcess(options) {
    const { inputStream, inFormat = 'jpg', outFormat = 'jpg', sourceFrame, targetFrame, targetAnchor, applyBottomGradient = false, bottomGradientExand = 1 } = options;
    if (!applyBottomGradient) {
        const cropProc = getCropProcess_1.default(inFormat, outFormat, sourceFrame, targetFrame, targetAnchor);
        inputStream.pipe(cropProc.stdin);
        return cropProc;
    }
    else {
        const cropProc = getCropProcess_1.default(inFormat, 'miff', sourceFrame, targetFrame, targetAnchor);
        inputStream.pipe(cropProc.stdin);
        const bottomGradientWidth = (targetFrame.width < targetFrame.height
            ? targetFrame.width
            : targetFrame.height) *
            targetFrame.margin[2] *
            bottomGradientExand;
        const gradientProc = getImgWithBottomGradientProcess_1.default('miff', targetFrame.width, bottomGradientWidth, outFormat);
        cropProc.stdout.pipe(gradientProc.stdin);
        return gradientProc;
    }
}
exports.default = getCropWithBottomGradientProcess;
//# sourceMappingURL=getCropWithBottomGradientProcess.js.map