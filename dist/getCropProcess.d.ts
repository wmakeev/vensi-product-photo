import execa from 'execa';
import { Frame } from '.';
import { Anchor } from './Anchor';
export default function getCropProcess(inFormat: 'miff' | 'png' | 'jpg', outFormat: 'miff' | 'png' | 'jpg', sourceFrame: Frame, targetFrame: Frame, targetAnchor: Anchor): execa.ExecaChildProcess<string>;
//# sourceMappingURL=getCropProcess.d.ts.map