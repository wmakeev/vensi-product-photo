import path from 'path'
import execa from 'execa'

import { Frame, FramePx, FrameMargin } from '.'
import { Anchor } from './Anchor'

const CONVERT = path.join(process.env.IM_PATH || '', 'convert')

const ADD = 1
// const SUBSTRACT = -1

const offset = (px: number) =>
  px < 0 ? `${Math.round(px)}` : `+${Math.round(px)}`

const getFrameInnerWidth = (frame: FramePx) =>
  frame.width - frame.marginPx[3] - frame.marginPx[1]

const getFrameInnerHeight = (frame: FramePx) =>
  frame.height - frame.marginPx[0] - frame.marginPx[2]

const getCropOpt = (frame: FramePx) => {
  const w = Math.round(getFrameInnerWidth(frame))
  const h = Math.round(getFrameInnerHeight(frame))
  return `${w}x${h}${offset(frame.marginPx[3])}${offset(frame.marginPx[0])}!`
}

const convertMarginToPx = (frame: Frame): FrameMargin => {
  const minSide = frame.width < frame.height ? frame.width : frame.height
  return [
    frame.margin[0] * minSide,
    frame.margin[1] * minSide,
    frame.margin[2] * minSide,
    frame.margin[3] * minSide
  ]
}

/** Корректируем границы рамки (расширяем (по умолчанию) или уменьшаем) */
const applyMarginToFrame = (
  frame: FramePx,
  margin: FrameMargin,
  scale = 1,
  sigW = ADD,
  sigH = sigW
) =>
  ({
    ...frame,
    marginPx: [
      frame.marginPx[0] - sigH * margin[0] * scale,
      frame.marginPx[1] - sigW * margin[1] * scale,
      frame.marginPx[2] - sigH * margin[2] * scale,
      frame.marginPx[3] - sigW * margin[3] * scale
    ]
  } as FramePx)

/** Вписывание широкой рамки в узкую */
const expandInnerFrameHeight = (
  frame: FramePx,
  value: number,
  anchor = Anchor.Bottom
): FramePx => {
  const frameMargin = [...frame.marginPx] as FrameMargin

  switch (anchor) {
    case Anchor.BottomLeft:
    case Anchor.Bottom:
    case Anchor.BottomRight:
      frameMargin[0] = frameMargin[0] - value
      break

    case Anchor.MiddleLeft:
    case Anchor.Center:
    case Anchor.MiddleRight:
      frameMargin[0] = frameMargin[0] - value / 2
      frameMargin[2] = frameMargin[2] - value / 2
      break

    case Anchor.TopLeft:
    case Anchor.Top:
    case Anchor.TopRight:
      frameMargin[2] = frameMargin[2] - value
      break

    default:
      throw new Error(`Unknown anchor '${anchor}'`)
  }

  return { ...frame, marginPx: frameMargin }
}

/** Вписывание высокой рамки в широкую */
const expandInnerFrameWidth = (
  frame: FramePx,
  value: number,
  anchor = Anchor.Bottom
): FramePx => {
  const frameMargin = [...frame.marginPx] as FrameMargin

  switch (anchor) {
    case Anchor.TopLeft:
    case Anchor.MiddleLeft:
    case Anchor.BottomLeft:
      frameMargin[1] = frameMargin[1] - value
      break

    case Anchor.Top:
    case Anchor.Center:
    case Anchor.Bottom:
      frameMargin[1] = frameMargin[1] - value / 2
      frameMargin[3] = frameMargin[3] - value / 2
      break

    case Anchor.TopRight:
    case Anchor.MiddleRight:
    case Anchor.BottomRight:
      frameMargin[3] = frameMargin[3] - value
      break

    default:
      throw new Error(`Unknown anchor '${anchor}'`)
  }

  return { ...frame, marginPx: frameMargin }
}

// TODO Задавать не размер а соотношение сторон. Размер можно подогнать в другом потоке.
export default function getCropProcess(
  inFormat: 'miff' | 'png' | 'jpg',
  outFormat: 'miff' | 'png' | 'jpg',
  sourceFrame: Frame,
  targetFrame: Frame,
  targetAnchor: Anchor
) {
  const sourceFramePx: FramePx = {
    height: sourceFrame.height,
    width: sourceFrame.width,
    marginPx: convertMarginToPx(sourceFrame)
  }

  const targetFramePx: FramePx = {
    height: targetFrame.height,
    width: targetFrame.width,
    marginPx: convertMarginToPx(targetFrame)
  }

  /** Ширина объекта в пикселях для source рамки */
  const sw = getFrameInnerWidth(sourceFramePx)
  /** Высота объекта в пикселях для source рамки */
  const sh = getFrameInnerHeight(sourceFramePx)

  if (sw <= 0 || sh <= 0) {
    throw new Error('Границы исходной рамки перекрывают всю область объекта')
  }

  /** Ширина объекта в пикселях для target рамки */
  const tw = getFrameInnerWidth(targetFramePx)
  /** Высота объекта в пикселях для target рамки */
  const th = getFrameInnerHeight(targetFramePx)

  if (tw <= 0 || th <= 0) {
    throw new Error('Границы целевой рамки перекрывают всю область объекта')
  }

  /** [Source object width px] / [Source object height px] */
  const srcFrameInnerAspect = sw / sh
  /** [Target object width px] / [Target object height px] */
  const tarFrameInnerAspect = tw / th

  /** Коэфициент соотношения форм target и source объектов */
  const kFrameAspect = tarFrameInnerAspect / srcFrameInnerAspect

  // Идея: Берем границу объекта и расширяем её так, чтобы она соотв. по форме
  // нужной границе с нужными отступами.
  // Затем обрезаем исходные отступы, оставляя только границу в которой уже
  // выставлены нужные рамки на предыдущем шаге.

  // Расширяем границы объекта srcFrame с учетом якоря, так, чтобы они соотв.
  // целевым границам в tarFrame (исходные отступы margin фак-ки не меняются).
  let expandedFrame: FramePx
  let kFrame: number

  if (kFrameAspect < 1) {
    kFrame = sw / tw

    const dTarget = th - sh / kFrame
    const hrzExpand = dTarget * kFrame

    expandedFrame = expandInnerFrameHeight(
      sourceFramePx,
      hrzExpand,
      targetAnchor
    )
  } else {
    kFrame = sh / th

    const dTarget = tw - sw / kFrame
    const hrzExpand = dTarget * kFrame

    expandedFrame = expandInnerFrameWidth(
      sourceFramePx,
      hrzExpand,
      targetAnchor
    )
  }

  // .. далее продолжаем расширять границы объекта, добавляя к ним уже
  // рамки (margin) целевого объекта.
  const resultFrame = applyMarginToFrame(
    expandedFrame,
    targetFramePx.marginPx,
    kFrame
  )

  // Теперь осталось отбросить исходные отступы.
  // В результате получим объект с отступами как в tarFrame.
  const cropOpt = getCropOpt(resultFrame)
  const resizeFrame = `${targetFramePx.width}x${targetFramePx.height}`

  const command = [
    // file,
    `${inFormat}:-`,
    // crop
    '-crop',
    cropOpt,
    // background
    '-background',
    'white',
    // flatten
    '-flatten',
    // resize
    '-resize',
    resizeFrame,
    // sharpen
    '-sharpen',
    '3',
    // out
    `${outFormat}:-`
  ]

  return execa(CONVERT, command)
}

/*
-gravity ${getImGravity(anchor)} \
  -extent ${resizeFrame} \
*/

// TODO unfold нужно не вписывать, а кропать
