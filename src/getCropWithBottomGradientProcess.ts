import getCropProcess from './getCropProcess'
import getImgWithBottomGradientProcess from './getImgWithBottomGradientProcess'

import { Frame, Anchor } from '.'
import { Readable } from 'stream'

type Options = {
  inputStream: Readable
  inFormat?: 'miff' | 'png' | 'jpg'
  outFormat?: 'miff' | 'png' | 'jpg'
  sourceFrame: Frame
  targetFrame: Frame
  targetAnchor: Anchor
  applyBottomGradient?: boolean
  bottomGradientExand?: number
}

export default function getCropWithBottomGradientProcess(options: Options) {
  const {
    inputStream,
    inFormat = 'jpg',
    outFormat = 'jpg',
    sourceFrame,
    targetFrame,
    targetAnchor,
    applyBottomGradient = false,
    bottomGradientExand = 1
  } = options

  if (!applyBottomGradient) {
    const cropProc = getCropProcess(
      inFormat,
      outFormat,
      sourceFrame,
      targetFrame,
      targetAnchor
    )

    inputStream.pipe(cropProc.stdin!)

    return cropProc
  } else {
    const cropProc = getCropProcess(
      inFormat,
      'miff',
      sourceFrame,
      targetFrame,
      targetAnchor
    )

    inputStream.pipe(cropProc.stdin!)

    const bottomGradientWidth =
      (targetFrame.width < targetFrame.height
        ? targetFrame.width
        : targetFrame.height) *
      targetFrame.margin[2] *
      bottomGradientExand

    const gradientProc = getImgWithBottomGradientProcess(
      'miff',
      targetFrame.width,
      bottomGradientWidth,
      outFormat
    )

    cropProc.stdout!.pipe(gradientProc.stdin!)

    return gradientProc
  }
}
