import path from 'path'
import execa from 'execa'
import { Writable } from 'stream'

// import { spawn } from 'child_process'
// const execa = spawn

const CONVERT = path.join(process.env.IM_PATH || '', 'convert')

export default function getImgWithBottomGradientProcess(
  srcImgFormat: 'miff' | 'png' | 'jpg' = 'miff',
  srcImgWidth: number,
  bottomGradientWidthPx: number,
  outImgFormat: 'miff' | 'png' | 'jpg' = 'jpg'
) {
  const minGradWidth = Math.round(srcImgWidth * 0.01) || 10

  if (bottomGradientWidthPx < minGradWidth) {
    bottomGradientWidthPx = minGradWidth
  }

  const gradientMiffStream = execa(CONVERT, [
    `-size`,
    `${Math.round(srcImgWidth)}x${Math.round(bottomGradientWidthPx)}`,
    `gradient:`,
    `miff:-`
  ]).stdout!

  const resultChildProc = execa(
    CONVERT,
    [
      `${srcImgFormat}:-`,
      'fd:3',
      '-gravity',
      'South',
      '-alpha',
      'off',
      '-compose',
      'CopyOpacity',
      '-composite',
      '-background',
      'white',
      '-alpha',
      'remove',
      `${outImgFormat}:-`
    ],
    {
      stdio: ['pipe', 'pipe', 2, 'pipe']
    }
  )

  gradientMiffStream.pipe(resultChildProc.stdio[3] as Writable)

  return resultChildProc
}
