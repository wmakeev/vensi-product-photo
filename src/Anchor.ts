export enum Anchor {
  TopLeft = 'TopLeft',
  Top = 'Top',
  TopRight = 'TopRight',
  MiddleLeft = 'MiddleLeft',
  Center = 'Center',
  MiddleRight = 'MiddleRight',
  BottomLeft = 'BottomLeft',
  Bottom = 'Bottom',
  BottomRight = 'BottomRight'
}

export function getAnchor(anchor: string) {
  switch (anchor) {
    case 'TopLeft':
      return Anchor.TopLeft

    case 'Top':
      return Anchor.Top

    case 'TopRight':
      return Anchor.TopRight

    case 'MiddleLeft':
      return Anchor.MiddleLeft

    case 'Center':
      return Anchor.Center

    case 'MiddleRight':
      return Anchor.MiddleRight

    case 'BottomLeft':
      return Anchor.BottomLeft

    case 'Bottom':
      return Anchor.Bottom

    case 'BottomRight':
      return Anchor.BottomRight

    default:
      throw new Error(`Unknown Anchor type ${anchor}`)
  }
}

export function getImageMagickGravity(anchor: Anchor) {
  switch (anchor) {
    case Anchor.TopLeft:
      return 'NorthWest'

    case Anchor.Top:
      return 'North'

    case Anchor.TopRight:
      return 'NorthEast'

    case Anchor.MiddleLeft:
      return 'West'

    case Anchor.Center:
      return 'Center'

    case Anchor.MiddleRight:
      return 'East'

    case Anchor.BottomLeft:
      return 'SouthWest'

    case Anchor.Bottom:
      return 'South'

    case Anchor.BottomRight:
      return 'SouthEast'

    default:
      throw new Error(`Unknown Anchor type ${anchor}`)
  }
}
