import fs from 'fs'
import path from 'path'
import test from 'blue-tape'

import { FrameMargin, Anchor } from '..'
import { getCropProcess } from '..'

const IMG_FOLDER = path.resolve(__dirname, '../../img')
const TMP_FOLDER = path.resolve(__dirname, '../../__temp')

test.only('crop width gradient #1', t => {
  t.plan(1)

  const photoReadStream = fs.createReadStream(
    path.resolve(IMG_FOLDER, '65473-front.jpg')
  )

  const photoWriteStream = fs.createWriteStream(
    path.resolve(TMP_FOLDER, '65473-front-out.jpg')
  )

  const sourceFrame = {
    width: 1901,
    height: 2500,
    margin: [0.149, 0.151, 0.164, 0.134] as FrameMargin
  }

  const targetFrame = {
    width: 300,
    height: 400,
    margin: [0.03, 0.06, 0.06, 0.06] as FrameMargin
  }

  const cropProc = getCropProcess({
    inputStream: photoReadStream,
    sourceFrame,
    targetFrame,
    targetAnchor: Anchor.Bottom,
    applyBottomGradient: true,
    bottomGradientExand: 1.1
  })

  cropProc.stdout?.pipe(photoWriteStream)

  cropProc
    .then(() => {
      t.pass('should pass')
    })
    .catch(err => {
      t.fail(err.message)
    })
})
