import fs from 'fs'
import path from 'path'
import test from 'blue-tape'

import getImgWithBottomGradientProcess from '../getImgWithBottomGradientProcess'

const IMG_FOLDER = path.resolve(__dirname, `../../img`)
const TMP_FOLDER = path.resolve(__dirname, `../../__temp`)

test('getImgStreamWithBottomGradient', t => {
  t.plan(1)

  const srcPhotoStream = fs.createReadStream(
    path.join(IMG_FOLDER, '00002-front.jpg')
  )

  const resultPrc = getImgWithBottomGradientProcess('jpg', 1500, 120, 'jpg')

  const writeStream = fs.createWriteStream(
    path.join(TMP_FOLDER, '00002-front-gradient.jpg')
  )

  srcPhotoStream.on('close', () => {
    t.pass('DONE')
  })

  srcPhotoStream.pipe(resultPrc.stdin!)
  resultPrc.stdout!.pipe(writeStream)
})
