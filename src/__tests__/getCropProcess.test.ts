import test from 'blue-tape'
import fs from 'fs'
import path from 'path'
import { Readable, Writable } from 'stream'

import { Anchor, Frame, FrameMargin } from '..'
import getCropProcess from '../getCropProcess'

const IMG_FOLDER = path.resolve(__dirname, '../../img')
const TMP_FOLDER = path.resolve(__dirname, '../../__temp')

const IMG_WIDTH = 549
const IMG_HEIGHT = 732
const CELL_PX = 18.3

const getMarginValue = (cells: number) => (cells * CELL_PX) / IMG_WIDTH

// TODO Сделать встроенные авто-тесты по кейсам. Автоматически выделять границу и сравнивать.

test('crop #1', t => {
  const FILE_NAME = 'graph-paper-6x8-blue'
  t.plan(1)

  const inFile = path.resolve(IMG_FOLDER, `.${FILE_NAME}.jpg`)
  const inFileStream: Readable = fs.createReadStream(inFile)

  const outFile1 = path.resolve(TMP_FOLDER, `${FILE_NAME}_01.jpg`)
  const outFile1Stream: Writable = fs.createWriteStream(outFile1)

  const sourceFrame: Frame = {
    width: IMG_WIDTH,
    height: IMG_HEIGHT,
    margin: [
      getMarginValue(15),
      getMarginValue(5),
      getMarginValue(5),
      getMarginValue(10)
    ]
  }

  const targetFrame: Frame = {
    width: Math.round(IMG_WIDTH / 2),
    height: Math.round(IMG_HEIGHT / 2), // target уже source
    margin: [
      getMarginValue(4),
      getMarginValue(2),
      getMarginValue(3),
      getMarginValue(6)
    ]
  }

  const cropProcess = getCropProcess(
    'jpg',
    'jpg',
    sourceFrame,
    targetFrame,
    Anchor.Bottom
  )

  cropProcess.stdout?.on('close', () => t.pass())

  cropProcess.stdout?.pipe(outFile1Stream)

  inFileStream.pipe(cropProcess.stdin!)
})

test('crop #2 (fail)', t => {
  t.plan(1)

  const sourceFrame = {
    width: 1901,
    height: 2500,
    margin: [
      0.17674907943187795,
      0.12204103103629668,
      0.20305102577590742,
      0.12204103103629668
    ] as FrameMargin
  }

  const targetFrame = {
    width: 1500,
    height: 2000,
    margin: [0.5, 0.5, 0.5, 0.5] as FrameMargin
  }

  try {
    getCropProcess('jpg', 'jpg', sourceFrame, targetFrame, Anchor.Bottom)
  } catch (err) {
    t.equal(
      err.message,
      'Границы целевой рамки перекрывают всю область объекта',
      'should fail'
    )
  }
})

test.only('crop #3', t => {
  t.plan(1)

  const photoReadStream = fs.createReadStream(
    path.resolve(IMG_FOLDER, '65473-front.jpg')
  )

  const photoWriteStream = fs.createWriteStream(
    path.resolve(TMP_FOLDER, '65473-front-out.jpg')
  )

  const sourceFrame = {
    width: 1901,
    height: 2500,
    margin: [0.149, 0.151, 0.164, 0.134] as FrameMargin
  }

  const targetFrame = {
    width: 150,
    height: 200,
    margin: [0.0, 0.0, 0.0, 0.0] as FrameMargin
  }

  const cropProc = getCropProcess(
    'jpg',
    'jpg',
    sourceFrame,
    targetFrame,
    Anchor.Bottom
  )

  cropProc.stdout?.pipe(photoWriteStream)
  photoReadStream.pipe(cropProc.stdin!)

  cropProc
    .then(() => {
      t.pass('should pass')
    })
    .catch(err => {
      t.fail(err.message)
    })
})
